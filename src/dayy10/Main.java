package dayy10;

import java.math.BigDecimal;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {
		System.out.println("Penyelesaian BIG SORTING");

		System.out.println();
		System.out.print("Masukan banyak bilangan yang ingin diinputkan: ");
		int n = input.nextInt();

		BigDecimal[] bilangan = new BigDecimal[n];
		int i = 0;
		for (i = 0; i < n; i++) {
			System.out.print("Masukan bilangan ke-" + (i + 1) + " : ");
			bilangan[i] = input.nextBigDecimal();
		}

		for (i = 0; i < bilangan.length; i++) {
			for (int j = 0; j < bilangan.length; j++) {
				if (bilangan[i].compareTo(bilangan[j]) == -1) {
					BigDecimal tampungan = bilangan[i];
					bilangan[i] = bilangan[j];
					bilangan[j] = tampungan;
				}
			}
		}

		for (i = 0; i < n; i++) {
			System.out.print(bilangan[i] + "\t");
		}
	}

	private static void soal2() {
		System.out.println("Penyelesaian INSERTION SORT");

		System.out.print("Masukan banyak angka: ");
		int n = input.nextInt();
		int i = 0;

		int[] bilangan = new int[n];
		for (i = 0; i < n; i++) {
			System.out.print("Masukan angka ke-" + (i + 1));
			bilangan[i] = input.nextInt();
		}
		int tampungan = bilangan[(bilangan.length)-1];
		for(i=(bilangan.length-2); i>=0; i--) {
			if(bilangan[i]>tampungan) {
				bilangan[i+1] = bilangan[i];
			}else {
				bilangan[i+1] = tampungan;
				break;
			}
			if(i==0 && bilangan[i] > tampungan) {
				bilangan[i] = tampungan;
			}
			for(int j=0; j<bilangan.length; j++) {
			System.out.print(bilangan[j] + "  ");
			}
		System.out.println();
		}
		for(int j=0; j<bilangan.length; j++) {
		System.out.print(bilangan[j] + "  ");
		}
	}

	private static void soal3() {
		System.out.println("Penyelesaian CORRECTNESS AND THE LOOP INVARIANT");
		System.out.println();

		System.out.print("Masukan panjang inputan: ");
		int n = input.nextInt();
		int[] bilangan = new int[n];
		for (int i = 0; i < n; i++) {
			System.out.print("Masukan angka ke-" + (i + 1) + " : ");
			bilangan[i] = input.nextInt();
		}

		for (int a = 1; a < bilangan.length; a++) {
			for (int b = a; b >= 0; b--) {
				if (b > 0 && bilangan[b] < bilangan[b - 1]) {
					int tampungan = bilangan[b];
					bilangan[b] = bilangan[b - 1];
					bilangan[b - 1] = tampungan;
				}
			}
		}
		for (int j = 0; j < bilangan.length; j++) {
			System.out.print(bilangan[j] + "\t");
		}

	}

	private static void soal4() {
		System.out.print("Penyelesaian RUNNING TIME OF ALGORITHMS");
		
		System.out.println();
		System.out.print("Masukan panjang inputan : ");
		int n = input.nextInt();							//n=3
		int i=0;
		int[] bilangan = new int [n];
		for(i=0; i<n; i++) {
			System.out.print("Masukan bilangan ke-" + (i+1) + " : ");
			bilangan[i] = input.nextInt();					//2,3,1
		}
		int count=0;
		for(i=0; i<bilangan.length; i++) {					//i=0		i=1							i=2								i=2
			for(int j=i; j>0; j--) {						//j=0		j=1							j=2								j=1
				if(bilangan[j-1] > bilangan[j]) {			//			bil[0] = 2 < bil[1] = 3		bil[1] = 3 > bil[2] = 1			bil[0] = 2 > bil[1] = 1
					int tampungan = bilangan[j-1];			//			 							tamp = 3						tamp = 2
					bilangan[j-1] = bilangan[j];			//										bil[1] = 1						bil[0] = 1
					bilangan[j] = tampungan;				//										bil[2] = 3						bil[1] = 2
					count++;								//										count = 1						count = 2
				}
			}
//			for(int a=0; a<bilangan.length; a++) {
//				System.out.print(bilangan[a] + " ");
//			}
//			System.out.println();
		}
		System.out.println(count);							//2
	}

	private static void soal5() {
		System.out.println("Penyelesaian soal COUNTING SORT 1");
		
		System.out.println();
		
		System.out.print("Masukan banyak bilangan : ");
		int n = input.nextInt();
		input.nextLine();
		System.out.print("Masukan bilangan : ");
		String bilangan = input.nextLine();
		
		String[] bil = bilangan.split(" ");

		int i =0;
		
		int[] hitung = new int [n];
		for (i = 0; i < n; i++) {
			int elemen = Integer.parseInt(bil[i]);
			hitung[elemen]++;
		}
		
		for(i=0; i<n; i++) {
			System.out.print(hitung[i] + "  ");
		}
	}

	private static void soal6() {
		System.out.println("Penyelesaian soal COUNTING SORT 2");
		
		System.out.println();
		System.out.print("Masukan banyak bilangan: ");
		int n = input.nextInt();
		input.nextLine();
		System.out.print("Masukan bilangan: ");
		String bilangan = input.nextLine();
		
		String[] bil = bilangan.split(" ");
		int[] intbil = new int[bil.length];
		
		for(int a=0; a<bil.length; a++) {
			intbil[a] = Integer.parseInt(bil[a]);
		}
		
		for(int i=0; i < n; i++) {
			for(int j=i; j>0; j--) {
				if(intbil[j-1] > intbil[j]) {
					int tampungan = intbil[j-1];
					intbil[j-1] = intbil[j];
					intbil[j] = tampungan;
				}
			}
			
		}
		for(int k =0; k<n; k++) {
			System.out.print(intbil[k] + "  ");
		}
	}

	private static void soal7() {
		System.out.println("Penyelesaian soal FIND THE MEDIAN");
		
		System.out.println();
		
		System.out.print("Masukan panjang inputan : ");
		int n = input.nextInt();
		
		int i=0;
		int[] bilangan = new int [n-1];
		for(i=0; i<n; i++) {
			System.out.print("Masukan bilangan ke-" + (i+1) + " : ");
			bilangan[i] = input.nextInt();
		}
		
		for (int a = 1; a < bilangan.length; a++) {
			for (int b = a; b >= 0; b--) {
				if (b > 0 && bilangan[b] < bilangan[b - 1]) {
					int tampungan = bilangan[b];
					bilangan[b] = bilangan[b - 1];
					bilangan[b - 1] = tampungan;
				}
			}
		}
		float median=0;
		int med =0;
		if(n%2==0) {
			median = bilangan[(n-1)/2]+bilangan[((n-1)/2)+1];
			median = median/2;
			System.out.println(median);
		}
		if(n%2==1) {
			med = bilangan[(int) Math.ceil(n/2)];
			System.out.println(med);
		}
		
		
	}

	private static void soal8() {
		System.out.println("Penyelesaian soal SORTING THE ALFABET");
		
		System.out.print("Masukan alfabet: ");
		String alfabet = input.next();
		
		char[] charalfabet = alfabet.toCharArray();
		
		for(int i=0; i<charalfabet.length; i++) {
			for(int j=i; j>0; j--) {
				if(charalfabet[j-1] > charalfabet[j]) {
					char temp = charalfabet[j-1];
					charalfabet[j-1]=charalfabet[j];
					charalfabet[j] = temp;
				}
			}
		}
		
		for(int i=0; i<charalfabet.length; i++) {
			System.out.print(charalfabet[i]);
		}
	}

}
