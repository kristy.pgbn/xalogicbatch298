package oop;

import java.util.Scanner;

public class Player {
	static Scanner input = new Scanner(System.in);
	String name;
	int point = 30;
	int taruhanpemain1 = 0;
	int taruhanpemain2 = 0;
	int pointpemain1 = 0;
	int pointpemain2 = 0;
	String tebakanpemain1 = "";
	String tebakanpemain2 = "";

	String hasilpemain1 = "";
	String hasilpemain2 = "";

	int angkaRandom = 0;

	String pilihan = "";
	int i = 1;

	String namapemain2 = "";

	public Player(String name) {
		this.name = name;
	}

	public void tanding(Player pemain) {
		System.out.println("****************************************************************");
		System.out.println("\t\t" + "Pemain 1 (" + this.name + ") vs Pemain 2 (" + pemain.name + ") ");
		System.out.println("****************************************************************");
		System.out.println("\t" + "Poin awal masing-masing pemain adalah " + this.point + " poin");

		pointpemain1 = this.point;
		pointpemain2 = pemain.point;
		namapemain2 = pemain.name;

		pilihan = "Y";
		while (pilihan.toUpperCase().equals("Y")) {
			angkaRandom = (int) (Math.random() * 10);
			System.out.println(angkaRandom);
			System.out.println();
			System.out.println("----------------------------------------------------------------");
			System.out.println("\t\t\t" + "    Babak " + i);
			System.out.println("----------------------------------------------------------------");
			System.out.println("Pemain 1");
			System.out.print("Masukan taruhan: ");
			taruhanpemain1 = input.nextInt();
			System.out.print("Masukan tebakan: ");
			tebakanpemain1 = input.next();
			System.out.println();
			System.out.println("Pemain 2");
			System.out.print("Masukan taruhan: ");
			taruhanpemain2 = input.nextInt();
			System.out.print("Masukan tebakan: ");
			tebakanpemain2 = input.next();

			hasil();

			if (pilihan.toUpperCase().equals("N")) {
				System.out.println();
				System.out.println("Permainan berakhir");
				System.out.println("----------------------------------------------------------------");
				break;
			}
		}

	}

	public void hasil() {
		if (taruhanpemain1 <= pointpemain1 && taruhanpemain2 <= pointpemain2 && taruhanpemain1 >= 0
				&& taruhanpemain2 >= 0 && (tebakanpemain1.equals("D") || tebakanpemain2.equals("D")
						|| tebakanpemain1.equals("U") || tebakanpemain2.equals("U"))) {
			if (angkaRandom > 5) {
				if (tebakanpemain1.equals(tebakanpemain2) && tebakanpemain2.equals("D")) {
					pointpemain1 = pointpemain1 - taruhanpemain1;
					pointpemain2 = pointpemain2 - taruhanpemain2;
					hasilpemain1 = "You Lose";
					hasilpemain2 = "You Lose";
				}
				if (tebakanpemain1.equals(tebakanpemain2) && tebakanpemain2.equals("U")) {
					pointpemain1 = pointpemain1 - 0;
					pointpemain2 = pointpemain2 - 0;
					hasilpemain1 = "You Win";
					hasilpemain2 = "You Win";
				}
				if (tebakanpemain1.equals("D") && tebakanpemain2.equals("U")) {
					pointpemain1 = pointpemain1 - taruhanpemain1;
					pointpemain2 = pointpemain2 + taruhanpemain1;
					hasilpemain1 = "You Lose";
					hasilpemain2 = "You Win";
				}
				if (tebakanpemain1.equals("U") && tebakanpemain2.equals("D")) {
					pointpemain1 = pointpemain1 + taruhanpemain2;
					pointpemain2 = pointpemain2 - taruhanpemain2;
					hasilpemain1 = "You Win";
					hasilpemain2 = "You Lose";
				}
			}
			if (angkaRandom == 5) {
				if (tebakanpemain1.equals(tebakanpemain2) && tebakanpemain2.equals("D")
						|| tebakanpemain1.equals(tebakanpemain2) && tebakanpemain2.equals("U")
						|| tebakanpemain1.equals("D") && tebakanpemain2.equals("U")
						|| tebakanpemain1.equals("U") && tebakanpemain2.equals("D")) {
					pointpemain1 = pointpemain1 - 0;
					pointpemain2 = pointpemain2 - 0;
					hasilpemain1 = "SERI";
					hasilpemain2 = "SERI";
				}
			}
			if (angkaRandom < 5) {
				if (tebakanpemain1.equals(tebakanpemain2) && tebakanpemain2.equals("D")) {
					pointpemain1 = pointpemain1 - 0;
					pointpemain2 = pointpemain2 - 0;
					hasilpemain1 = "You Win";
					hasilpemain2 = "You Win";
				}
				if (tebakanpemain1.equals(tebakanpemain2) && tebakanpemain2.equals("U")) {
					pointpemain1 = pointpemain1 - taruhanpemain1;
					pointpemain2 = pointpemain2 - taruhanpemain2;
					hasilpemain1 = "You Lose";
					hasilpemain2 = "You Lose";
				}
				if (tebakanpemain1.equals("D") && tebakanpemain2.equals("U")) {
					pointpemain1 = pointpemain1 + taruhanpemain2;
					pointpemain2 = pointpemain2 - taruhanpemain2;
					hasilpemain1 = "You Win";
					hasilpemain2 = "You Lose";
				}
				if (tebakanpemain1.equals("U") && tebakanpemain2.equals("D")) {
					pointpemain1 = pointpemain1 - taruhanpemain1;
					pointpemain2 = pointpemain2 + taruhanpemain1;
					hasilpemain1 = "You Lose";
					hasilpemain2 = "You Win";
				}
			}
			System.out.println();
			System.out.println("Pemain 1");
			System.out.println("Sisa Poin = " + pointpemain1);
			System.out.println("Hasil = " + hasilpemain1);
			System.out.println();
			System.out.println("Pemain 2");
			System.out.println("Sisa Poin = " + pointpemain2);
			System.out.println("Hasil = " + hasilpemain2);

			if (pointpemain1 != 0 && pointpemain2 != 0) {
				pilihan = "Y";
				i++;
			} else {
				System.out.println();
				if (pointpemain1 == 0) {
					System.out.println();
					System.out.println("Pemenang adalah Pemain 2 (" + namapemain2 + ")");
					pilihan = "N";
				} else {
					System.out.println();
					System.out.println("Pemenang adalah Pemain 1 (" + this.name + ")");
					pilihan = "N";
				}
			}
		} else {
			System.out.println();
			System.out.println("Periksa kembali besar taruhan atau tebakan!");
			System.out.println("Ingin melanjutkan? (Y/N)");
			pilihan = input.next();

		}
	}
}