package oop;

import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("================================================================");
		System.out.println(" Permainan menebak angka acak (Lebih besar(U)/ Lebih kecil(D))");
		System.out.println("================================================================");
		System.out.println("Pemain 1");
		System.out.print("Masukan nama: ");
		String nama = input.next();
		Player pemain1 = new Player(nama);

		System.out.println();

		System.out.println("Pemain 2");
		System.out.print("Masukan nama: ");
		String nama1 = input.next();
		Player pemain2 = new Player(nama1);

		System.out.println();
		pemain1.tanding(pemain2);
	}
}
