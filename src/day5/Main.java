package day5;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}

	}

	private static void soal1() {
		System.out.println("Disini soal 1");
		System.out.println("=========================================");
		System.out.println();
		System.out.print("Masukan jumlah uang: ");
		int uang = input.nextInt();
		System.out.println();

		System.out.print("Masukan harga baju: ");
		String hargabaju = input.next();
		System.out.println();

		System.out.print("Masukan harga celana: ");
		String hargacelana = input.next();

		String[] arraybaju = hargabaju.split(",");
		String[] arraycelana = hargacelana.split(",");

		int[] arraybaju1 = new int[arraybaju.length];
		int[] arraycelana1 = new int[arraycelana.length];

		int total = 0;

		int maksimum = 0; // tidak perlu bentuk array

		for (int i = 0; i < arraybaju.length; i++) {
			arraybaju1[i] = Integer.parseInt(arraybaju[i]);
		}
		for (int j = 0; j < arraycelana.length; j++) {
			arraycelana1[j] = Integer.parseInt(arraycelana[j]);
		}

		for (int a = 0; a < arraybaju1.length; a++) {
			for (int b = 0; b < arraycelana1.length; b++) {
				total = arraybaju1[a] + arraycelana1[b]; // total[0,0] = 75; total[0,1] = 65
				if (total > maksimum && total <= uang) { // maks = 0 < total = 75
					maksimum = total; // maks = 75
				}
			}
		}
		System.out.println(maksimum);
	}

	private static void soal2() {
		System.out.println("Disini soal 2");
		System.out.println();

		System.out.print("Masukan elemen array: ");
		String array = input.next();

		System.out.print("Masukan banyak rotasi: ");
		int rotasi = input.nextInt();

		String[] array1 = array.split(",");

		int a = array1.length - 1;

		for (int i = 0; i < rotasi; i++) {
			String hasil = "";
			String tampungan = array1[0];
			for (int j = 0; j < array1.length; j++) {
				if (j == 0) {
					tampungan = array1[j];
					array1[j] = array1[j + 1];
					hasil = hasil + array1[j] + ",";
				} else if (j == a) {
					array1[j] = tampungan;
					hasil = hasil + array1[j];
				} else {
					array1[j] = array1[j + 1];
					hasil = hasil + array1[j] + ",";
				}
			}
			System.out.println((i + 1) + " : " + hasil);
		}
	}

	private static void soal3() {
		System.out.println("Disini soal 3");
		System.out.println();
		System.out.print("Masukan banyak puntung yang dikumpulkan: ");
		int banyakPuntung = input.nextInt();
		int harga = 500;
		int batang = 8;

		int rakitan = banyakPuntung / batang;
		int total = harga * rakitan;
		System.out.println();
		System.out.println("Banyak rokok yang dapat dirakit oleh pemulung: " + rakitan);
		System.out.println("Penghasilan yang diperoleh adalah Rp " + total);
	}

	private static void soal4() {
		System.out.println("Disini soal 4");

		System.out.println();
		System.out.print("Masukan total menu: ");
		int x = input.nextInt();
		System.out.print("Masukan makanan alergi index ke- ");
		int y = input.nextInt();
		System.out.print("Masukan harga menu: ");
		String menu = input.next();
		System.out.print("Masukan uang yang dimiliki: ");
		int uang = input.nextInt();

		String[] menu1 = menu.split(",");

		int[] arraymenu = new int[menu1.length];
		
		if(x != menu1.length || x > (menu1.length-1)) {
			System.out.println("Banyak menu tidak sesusai dengan banyak harga menu.");
			return;
		}
		for (int i = 0; i < arraymenu.length; i++) {
			arraymenu[i] = Integer.parseInt(menu1[i]);
		}

		int total = 0;
		for (int j = 0; j < arraymenu.length; j++) {
			total = total + arraymenu[j];
		}
		total = total - arraymenu[y];
		total = total / 2;
		int sisa = uang - total;
		System.out.println("Total yang harus dibayarkan: " + total);
		if (sisa == 0) {
			System.out.println("Uang Pas");
		} else if (sisa > 0) {
			System.out.println("Sisa uang Anda: " + sisa);
		} else {
			System.out.println("Uang Anda " + sisa);
		}
	}

	private static void soal5() {
		System.out.println("Disini soal 5");
		System.out.println();
		
		System.out.print("Masukan kalimat: ");
		String kalimat = input.nextLine();
		
		String replacekalimat = kalimat.replaceAll(" ","");
		String lowerkalimat = replacekalimat.toLowerCase();
		char[] charkalimat = lowerkalimat.toCharArray();
		
		//untuk sorting bisa menggunakan Arrays.sort(variabel yang ingin di sorting)
		for(int a=0; a < charkalimat.length; a++) {
			for(int b=0; b < charkalimat.length; b++) {
				if(charkalimat[a]<charkalimat[b]) {
					char tampungan = charkalimat[a];
					charkalimat[a] = charkalimat[b];
					charkalimat[b] = tampungan;
				}
			}
		}
		
//		for(int i=0; i<charkalimat.length; i++) {
//			Arrays.sort(charkalimat);
//		}
		System.out.println();
		System.out.print("Vocal : ");
		for(int j=0; j<charkalimat.length; j++) {
			char karakter = charkalimat[j];
			if(karakter=='a' || karakter == 'i' || karakter == 'u' || karakter == 'e' || karakter == 'o'){
				char vocal= charkalimat[j];
				System.out.print(vocal);
			}
		}
		System.out.println();
		System.out.print("Konsonan : ");
		for(int k=0; k<charkalimat.length; k++) {
			char karakter = charkalimat[k];
			if(karakter=='a' || karakter == 'i' || karakter == 'u' || karakter == 'e' || karakter == 'o'){
				continue;
			} else {
				char konsonan= charkalimat[k];
				System.out.print(konsonan);
			}
		}
}

	private static void soal6() {
		System.out.println();
		System.out.println("Disini soal 6");
		System.out.println();
		
		System.out.print("Masukan kalimat: ");
		String words = input.nextLine();
		
		String words1 = words.toLowerCase();
		String[] pisah = words1.split(" ");
		
		for(int i = 0; i<pisah.length; i++) {
			String hasil="";
			char[] karakter = pisah[i].toCharArray();
			for(int j = 0; j<karakter.length; j++) {
			if(j%2==1) {
					karakter[j] ='*';
					hasil = hasil+karakter[j];
				} else {
					hasil = hasil+karakter[j];
				}
			}
			if(i<(pisah.length-1)) {
				hasil=hasil + " ";
			}
			System.out.print(hasil);
		}
	}
}
