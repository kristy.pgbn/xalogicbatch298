package day5;

public class StringManipulation {

	public static void main(String[] args) {
		// String manipulation
		
				String words1 = "Andre Muhammad Satrio";
				
				//memisahkan kalimat menjadi perkata (menggunakan split)
				String[] tempWords1 = words1.split(" ");
				System.out.println(tempWords1[tempWords1.length - 1] + " Panjang Array " + tempWords1.length);
				
				//mengubah kata menjadi karakter (menggunakan toCharArray)
				char[] charWords1 = tempWords1[1].toCharArray();
				System.out.println(charWords1[charWords1.length - 1]);
				
				//mengganti character dari kalimat
				String replaceWords1 = words1.replaceAll(" ", "");
				System.out.println(replaceWords1);
				
				//memecah kata menjadi kalimat character
				char[] charReplaceWords1 = replaceWords1.toCharArray();
				System.out.println(charReplaceWords1[17]);
				
				//mengambil nama "Muhammad"
				String muhammad = words1.substring(6,14);
				System.out.println(muhammad);
				
				//menyatukan kalimat menjadi kata dan huruf kecil semua
				String lowerWords1 = words1.toLowerCase().replaceAll(" ","");
				System.out.println(lowerWords1);
				
				//mengambil kata muhammad huruf kecil semua
				String tempMuhammad = lowerWords1.substring(5,13);
				System.out.println(tempMuhammad);
				
				//mengubah variabel loweWords1 menjadi character
				char[] tempLowerWords1 = lowerWords1.toCharArray();
				System.out.println(tempLowerWords1[0]);
				
				//mengubah huruf kecil jadi huruf kapital
				String upperWords1 = words1.toUpperCase();
				System.out.println(upperWords1);
	}

}
