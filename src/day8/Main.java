package day8;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {
		System.out.println("Penyelesaian soal LMS STRING nomor 1");

		System.out.println("Masukan kalimat: ");
		String words = input.next();

		char[] words1 = words.toCharArray();

		char[] words2 = words.toUpperCase().toCharArray();

		int camel = 1;

		for (int i = 0; i < words1.length; i++) {
			if (words1[i] == words2[i]) {
				camel++;
			}
		}
		System.out.println(camel);
	}

	private static void soal2() {
		System.out.println("Penyelesaian soal LMS STRING nomor 2");

		System.out.print("Masukan password: ");
		String password = input.next();

		char[] pass = password.toCharArray();

		String number = "0123456789";
		String lowerCase = "abcdefghijklmnopqrstuvwxyz";
		String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String specialChar = "!@#$%^&*()-+";

		char[] numbers = number.toCharArray();
		char[] lowercase = lowerCase.toCharArray();
		char[] uppercase = upperCase.toCharArray();
		char[] specialchar = specialChar.toCharArray();

		int sisa = 0;
		int hasil1 = 0;
		int hasil2 = 0;
		int hasil3 = 0;
		int hasil4 = 0;
		// #HackerRank

		if (password.length() < 6) {
			sisa = 6 - password.length();
			System.out.print("Password Anda kurang, tambahkan " + sisa + " karakter");
			return;
		}

		for (int i = 0; i < password.length(); i++) {
			for (int j = 0; j < number.length(); j++) {
				if (pass[i] == numbers[j]) {
					hasil1++;
					break;
				}
			}
			for (int k = 0; k < lowerCase.length(); k++) {
				if (pass[i] == lowercase[k]) {
					hasil2++;
					break;
				}
			}
			for (int a = 0; a < upperCase.length(); a++) {
				if (pass[i] == uppercase[a]) {
					hasil3++;
					break;
				}
			}
			for (int b = 0; b < specialChar.length(); b++) {
				if (pass[i] == specialchar[b]) {
					hasil4++;
					break;
				}
			}
		}

		int result = 0;
		if (hasil1 == 0) {
			result++;
		}
		if (hasil2 == 0) {
			result++;
		}
		if (hasil3 == 0) {
			result++;
		}
		if (hasil4 == 0) {
			result++;
		}
		System.out.println(result);
	}

	private static void soal3() {
		System.out.println("Penyelesaian SOAL LMS STRING nomor 3");

		System.out.println("Masukan panjang password: ");
		int n = input.nextInt();

		System.out.println("Masukan password: ");
		String password = input.next();

		System.out.println("Masukan banyak rotasi: ");
		int rotasi = input.nextInt();

		String kapital = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String hurufkecil = "abcdefghijklmnopqrstuvwxyz";
		String specialChar = "!@#$%^&*()-+";

		char[] kapital1 = kapital.toCharArray();
		char[] kapitalRotasi = kapital.toCharArray();

		char[] hurufkecil1 = hurufkecil.toCharArray();
		char[] hkrotasi = hurufkecil.toCharArray();

		char[] spchar = specialChar.toCharArray();

		char[] pass = password.toCharArray();
		char tampungan = 0;
		char tampungan1 = 0;

		if (n != password.length()) {
			System.out.println("Panjang password dan password yang dimasukan tidak sesuai.");
		} else {

			for (int i = 0; i < rotasi; i++) {
				for (int j = 0; j < kapitalRotasi.length; j++) {
					if (j == 0) {
						tampungan = kapitalRotasi[j];
						kapitalRotasi[j] = kapitalRotasi[j + 1];
						tampungan1 = hkrotasi[j];
						hkrotasi[j] = hkrotasi[j + 1];
					} else if (j == (kapitalRotasi.length - 1)) {
						kapitalRotasi[j] = tampungan;
						hkrotasi[j] = tampungan1;
					} else {
						kapitalRotasi[j] = kapitalRotasi[j + 1];
						hkrotasi[j] = hkrotasi[j + 1];
					}
				}
			}
			String hasil = "";
			for (int a = 0; a < password.length(); a++) {
				for (int b = 0; b < kapital.length(); b++) {
					if (pass[a] == kapital1[b]) {
						hasil = hasil + kapitalRotasi[b];
					} else if (pass[a] == hurufkecil1[b]) {
						hasil = hasil + hkrotasi[b];
					}
				}

				for (int c = 0; c < spchar.length; c++) {
					if (pass[a] == spchar[c]) {
						hasil = hasil + pass[a];
					}
				}
			}
			System.out.print(hasil);
		}
	}

	private static void soal4() {
		System.out.println("Soal dilewati!!");
	}

	private static void soal5() {
		System.out.println("Penyelesaian LMS STRING soal nomor 5");

		System.out.println("Masukan banyak kata: ");
		int jumlahkata = input.nextInt();

		String[] inputKata = new String[jumlahkata];
		int a = 0;
		for (a = 0; a < jumlahkata; a++) {
			System.out.print("Masukan kata ke-" + (a + 1) + " : ");
			inputKata[a] = input.next();
		}

		String hk = "hackerrank";
		int j = 0;
		for (a = 0; a < jumlahkata; a++) {
			j = 0;
			for (int i = 0; i < inputKata[a].length(); i++) {
				if (inputKata[a].charAt(i) == hk.charAt(j)) {
					j++;
				}
			}
			if (j == hk.length()) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}

		}

	}

	private static void soal6() {
		System.out.println("Penyelesaian LMS STRING soal nomor 6");

		System.out.print("Masukan kalimat: ");
		String kalimat = input.nextLine();

		kalimat = kalimat.replace(" ", "");

		kalimat = kalimat.toLowerCase();

		int j = 0;

		int[] hitung = new int[26];
		int jumlah = 0;

		for (int i = 0; i < kalimat.length(); i++) {
			char karakter = kalimat.charAt(i);
			hitung[karakter - 'a']++; // misal karakter = w maka range antara a ke w adalah 22 nanti hitung berindeks
										// 22
		}

		for (j = 0; j < hitung.length; j++) {
			if (hitung[j] > 0) {
				jumlah++;
			}
		}

		if (jumlah == 26) {
			System.out.println("Pangram");
		} else {
			System.out.println("Not Pangram");
		}
	}

	private static void soal7() {
		System.out.println("Penyelesaian soal LMS STRING nomor 7");

		System.out.println();
		System.out.print("Masukan banyak inputan: ");
		int n = input.nextInt();

		String[] inputan = new String[n];

		for (int i = 0; i < n; i++) {
			System.out.print("Masukan inputan ke-" + (i + 1) + " : ");
			inputan[i] = input.next();
		}
		
		String tampungan="";
		for (int i = 0; i < n; i++) {
			int bilanganpertama = 0;
			boolean beautifull = false;

			for (int j = 1; j <= (inputan[i].length() / 2); j++) {

				int bil = Integer.parseInt(inputan[i].substring(0, j));
				bilanganpertama = bil;

				tampungan = "" + bilanganpertama;
				for(int a=0; a<inputan[i].length(); a++) {
					if(tampungan.length()<inputan[i].length()) {
						bil++;
						tampungan = tampungan + bil;
					}
				}

				if (inputan[i].equals(tampungan)) {
					beautifull = true;
					break;
				}

			}
			if (beautifull) {
				System.out.println("YES " + bilanganpertama);
			} else {
				System.out.println("NO");
			}
		}
	}

	private static void soal8() {
		System.out.println("Penyelesaian soal LMS STRING nomor 8");

		System.out.print("Masukan banyak inputan: ");
		int n = input.nextInt();

		int i = 0;
		String[] inputan = new String[n];
		for (i = 0; i < n; i++) {
			System.out.print("Masukan inputan ke-" + (i + 1) + " : ");
			inputan[i] = input.next();
		}

		int count = 0;
		int[] tampungan = new int[26];

		for (i = 0; i < n; i++) {
			for (int j = 0; j < inputan[i].length(); j++) {
				char karakter = inputan[i].charAt(j);
				tampungan[karakter - 'a']++;
			}
		}
		for (i = 0; i < 26; i++) {
			if (tampungan[i] == n) {
				count++;
			}
		}
		System.out.println(count);
	}

	private static void soal9() {
		System.out.println("Penyelesaian soal LMS STRING nomor 9");

		int i;
		String[] inputan = new String[2];
		for (i = 0; i < 2; i++) {
			System.out.print("Masukan inputan ke-" + (i + 1) + " : ");
			inputan[i] = input.next();
		}

		int[] tampungan = new int[26];
		for (i = 0; i < 2; i++) {
			for (int j = 0; j < inputan[i].length(); j++) {
				char karakter = inputan[i].charAt(j);
				tampungan[karakter - 'a']++;
			}
		}
		int count = 0;
		for (i = 0; i < 26; i++) {
			if (tampungan[i] != 2 && tampungan[i] > 0) {
				count++;
			}
		}
		System.out.println(count);

	}

	private static void soal10() {
		System.out.println("Penyelesaian soal LMS STRING nomor 10");
		
		System.out.println();
		System.out.print("Masukan banyak kata yang dimasukan: ");
		int n = input.nextInt();
		
		int i;
		String[] kata = new String[2];
		for (i = 0; i < n; i++) {
			System.out.print("Masukan kata ke-" + (i + 1) + " : ");
			kata[i] = input.next();
		}
		
		int[] tampungan = new int[26];
		for (i = 0; i < n; i++) {
			for (int j = 0; j < kata[i].length(); j++) {
				char karakter = kata[i].charAt(j);
				tampungan[karakter - 'a']++;
			}
		}
		
		int hitung = 0;
		for (i = 0; i < 26; i++) {
			if (tampungan[i] == n) {
				hitung++;
			}
		}
		if(hitung>0) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}
}
