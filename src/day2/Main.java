package day2;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Input Hadiah (Pilih nomor 1 - 5) : ");
		
		int nomorHadiah = input.nextInt();
		
		switch(nomorHadiah) {
		case 1:
			System.out.println("Selamat anda mendapatkan mobil!");
			break;
		case 2:
			System.out.println("Selamat anda mendapatkan tiket  pulang ke Bandung!");
			break;
		case 3:
			System.out.println("Selamat anda mendapatkan boneka beruang!");
			break;
		case 4:
			System.out.println("Selamat anda mendapatkan liburan ke India!");
			break;
		case 5:
			System.out.println("Selamat anda mendapatkan motor Harley Jawa!");
			break;
		default:
			System.out.println("Nomor yang anda masukkan tidak sesuai");
			break;
		}
	}
	

}
