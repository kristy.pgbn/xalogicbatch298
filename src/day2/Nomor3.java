package day2;

import java.util.Scanner;

public class Nomor3 {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukkan belanja: ");
		int belanja = input.nextInt();
		System.out.println("Masukkan ongkos kirim: ");
		int ongkir = input.nextInt();
		
		int diskonOngkir;
		int diskonBelanja;
		int total;
		
		if(belanja >=30000 && belanja < 50000) {
			diskonOngkir = 5000;
			diskonBelanja = 5000;
		}else if(belanja >= 50000 && belanja< 100000){
			diskonOngkir = 10000;
			diskonBelanja = 10000;
		}else if(belanja >= 100000) {
			diskonOngkir = 10000;
			diskonBelanja = 20000;
		}else {
			diskonOngkir = 0;
			diskonBelanja = 0;
		}
		
		total = (belanja + ongkir)-(diskonOngkir+diskonBelanja);
		
		System.out.println("Belanja: " + belanja);
		System.out.println("Ongkos Kirim: " + ongkir);
		System.out.println("Diskon Ongkir: " + diskonOngkir);
		System.out.println("Diskon Belanja: " + diskonBelanja);
		System.out.println("Total Belanja: " + total);
	}
}
