package day2;

import java.util.Scanner;

public class Nomor1 {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan jumlah pulsa (minimal pembelian 10000): ");
		int pulsa = input.nextInt();
		String point = "";
		
		if(pulsa >=10000 && pulsa <25000) {
			point = "80";
		} else if(pulsa >=25000 && pulsa <50000) {
			point = "200";
		} else if(pulsa >=50000 && pulsa <100000) {
			point = "400";
		} else if(pulsa >= 100000) {
			point = "800";
		} else {
			point = "0";
		}
		System.out.println("Anda mendapatkan " + point + " poin " + " dari pengisian pulsa!");
	}
}
