package day2;

import java.util.Scanner;

public class Nomor2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukkan belanja: ");
		int belanja = input.nextInt();
		System.out.println("Masukkan jarak pengiriman (km): ");
		float jarak = input.nextFloat();
		System.out.println("Masukkan kode promo: ");
		String kodePromo = input.next();
		
		double diskon=belanja*0.4;
		double ongkir = 5000;
		double total;
		
		String voucher = "JKTOVO";
		
		if(belanja >= 30000 && voucher.equals(kodePromo)) {
			if(diskon<=30000) {
				diskon = diskon;
			} else {
				diskon = 30000;
			}
		} else {
			diskon = 0;
		} 
		if (jarak<= 5) {
			ongkir=ongkir;
		} else {
			ongkir = ongkir + ((jarak-5)*1000);
		}
		total = (belanja - diskon) + ongkir;
				
		System.out.println("Belanja: " + belanja);
		System.out.println("Diskon 40% : " + diskon);
		System.out.println("Ongkir : " + ongkir);
		System.out.println("Total Belanja : " + total);
	}
}
