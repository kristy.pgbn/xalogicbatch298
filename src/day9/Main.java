package day9;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {
		System.out.println("Penyelesaian soal latihan nomor 1");

		System.out.println();

		System.out.print("Masukan point: ");
		int point = input.nextInt();

		String pilihan = "Y";
		while (pilihan.toUpperCase().equals("Y")) {
			System.out.print("Masukan taruhan: ");
			int taruhan = input.nextInt();
			System.out.print("Tebak(U/D): ");
			String tebakan = input.next();

			int random = (int) (Math.random() * 10);
			String hasil = "";

			if (random > 5) {
				if (tebakan.equals("D")) {
					point = point - taruhan;
					hasil = "You Lose";
				}
				if (tebakan.equals("U")) {
					point = point + taruhan;
					hasil = "You Win";
				}
			}
			if (random == 5) {
				if (tebakan.equals("D") || tebakan.equals("U")) {
					hasil = "SERI";
				}
			}
			if (random < 5) {
				if (tebakan.equals("D")) {
					point = point + taruhan;
					hasil = "You Win";
				}
				if (tebakan.equals("U")) {
					point = point - taruhan;
					hasil = "You Lose";
				}
			}
			System.out.println(random);
			System.out.println(hasil);
			System.out.println(point);

			System.out.println();
			if (point != 0) {
				System.out.print("Lanjut permainan (y/n)? ");
				pilihan = input.next();
				if (pilihan.toUpperCase().equals("N")) {
					System.out.println("Permainan Selesai");
				}
			} else {
				System.out.println("Point Anda habis!");
				break;
			}

		}

	}

	private static void soal2() {
		System.out.println("Penyelesaian soal latihan nomor 2");

		System.out.println();

		int keranjang = 3;
		int i = 0;
		String[] isi = new String[3];
		int[] banyakbuah = new int[3];
		int total = 0;

		System.out.println("Masukan banyak buah dalam keranjang");
		for (i = 0; i < keranjang; i++) {
			System.out.print("Keranjang " + (i + 1) + " : ");
			isi[i] = input.next();
			if (isi[i].equals("kosong")) {
				isi[i] = "0";
			}
			banyakbuah[i] = Integer.parseInt(isi[i]);
			total = total + banyakbuah[i];
		}

		System.out.print("Keranjang ke berapa yang akan di bawa ke pasar (1/2/3)? ");
		String pilihan = input.next();

		String[] pilihan1 = pilihan.split(",");

//		int[] pilihanarray = new int[pilihan1.length];

		for (i = 0; i < pilihan1.length; i++) {
			int pilihanarray = Integer.parseInt(pilihan1[i]);
			total = total - banyakbuah[pilihanarray - 1];
		}

		if (total != 0) {
			System.out.println("Sisa buah: " + total);
		} else {
			System.out.println("Sisa buah: Tidak ada");
		}

	}

	private static void soal3() {
		System.out.println("Penyelesaian soal latihan nomor 3");

		System.out.println();

		System.out.print("Masukan banyak anak: ");
		int n = input.nextInt();
		long hasil = 1;

		for (int i = n; i > 0; i--) {
			hasil = hasil * i;
		}

		System.out.println(hasil);
	}

	private static void soal4() {
		System.out.println("Penyelesaian soal latihan nomor 4");

		System.out.println();

		System.out.print("Masukan banyak laki-laki dewasa: ");
		int pria = input.nextInt();
		System.out.print("Masukan banyak perempuan dewasa: ");
		int wanita = input.nextInt();
		System.out.print("Masukan banyak anak-anak: ");
		int anak = input.nextInt();
		System.out.print("Masukan banyak bayi: ");
		int bayi = input.nextInt();

		int total = (pria * 1) + (wanita * 2) + (anak * 3) + (bayi * 5);
		if (total % 2 == 1 && total > 10) {
			total = total + (wanita * 1);
		}
		System.out.println();
		System.out.println(total + " Baju");
	}

	private static void soal5() {
		System.out.println("Penyelesaian soal latihan nomor 5");

		System.out.println();

		System.out.print("Masukan banyak kue pukis: ");
		int n = input.nextInt();
		float tepungterigu = 125 / 15f;
		float gulapasir = 100 / 15f;
		float susumurni = 100 / 15f;
		float putihtelurayam = 100 / 15f;

		// bisa pakai decimal format jangan string format -> NumberFormat formater = new
		// DecimalFormat("#0.000")

		float total = n * (tepungterigu + gulapasir + susumurni + putihtelurayam);

		System.out.println("jumlah bahan yang diperlukan untuk membuat " + n + " kue pukis adalah: "
				+ String.format("%.3f", total) + " gram");
		System.out.println("Dengan rincian: ");
		System.out.println("Tepung terigu :" + String.format("%.3f", n * tepungterigu) + " gram");
		System.out.println("Gula pasir :" + String.format("%.3f", n * gulapasir) + " gram");
		System.out.println("Susu murni :" + String.format("%.3f", n * susumurni) + " gram");
		System.out.println("Putih telur ayam :" + String.format("%.3f", n * putihtelurayam) + " gram");
	}

	private static void soal6() throws ParseException {
		System.out.println("Penyelesaian soal latihan nomor 6");

		System.out.println();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy'T'HH.mm");

		System.out.print("Waktu masuk: ");
		String masuk = input.next();

		System.out.print("Waktu keluar: ");
		String keluar = input.next();

		Date waktumasuk = format.parse(masuk);
		Date waktukeluar = format.parse(keluar);

		long waktumasuk1 = waktumasuk.getTime();
		long waktukeluar1 = waktukeluar.getTime();

		long selisih = waktukeluar1 - waktumasuk1;

		float durasi = selisih / 3600000f; // 3600000 konversi jam ke milisekon

		double pembulatan = Math.ceil(durasi);
		int pembulatan1 = (int) pembulatan;
		int biayaparkir = 3000;

		int total = biayaparkir * pembulatan1;

		System.out.println("Biaya parkir : " + "Rp " + total);

	}

	private static void soal7() throws ParseException {
		System.out.println("Penyelesaian soal latihan nomor 7");

		System.out.println();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

		System.out.print("Tanggal peminjaman: ");
		String pinjam = input.next();

		System.out.print("Tanggal kembali: ");
		String kembali = input.next();

		Date tanggalpinjam = df.parse(pinjam);
		Date tanggalkembali = df.parse(kembali);

		long peminjaman = tanggalpinjam.getTime(); // hasil akan ke bentuk milisekon
		long pengembalian = tanggalkembali.getTime();

		long selisih = pengembalian - peminjaman;

		long lamawaktu = selisih / (24 * 60 * 60 * 1000); // 24 jam, 60 menit, 60 detik, 1000 milisekon

		long durasipinjam = lamawaktu - 3;

		int biayadenda = 500;
		long denda = durasipinjam * biayadenda;

		System.out.println("Denda " + denda);

	}

	private static void soal8() {
		// baru inputan satu
		System.out.println("Penyelesaian soal latihan nomor 8");

		System.out.print("Masukan banyak kata :");
		int n = input.nextInt();
		int a = 0;
		String[] kata = new String[n];
		for (a = 0; a < n; a++) {
			System.out.print("Masukan kata ke-" + (a+1) + " : ");
			kata[a] = input.next().toLowerCase();
		}

		System.out.println();
		for (a = 0; a < n; a++) {
			if (kata[a].equals("bakso") || kata[a].equals("sop")) {
				System.out.println(kata[a] + "\t" + "Tumpah");
			} else {
				int jumlah=0;
				int[] tampungan = new int[26];
				for (int i = 0; i < kata[a].length(); i++) {
					char karakter = kata[a].charAt(i);
					tampungan[karakter - 'a']++;
				}

				for (int j = 0; j < 26; j++) {
					if (tampungan[j] > 1) {
						jumlah++;
					}
				}

				if (jumlah == (kata[a].length() / 2)) {
					System.out.println(kata[a] + "\t" + "Yes");
				} 
				if (jumlah != (kata[a].length()/2)){
					System.out.println(kata[a] + "\t" + "No");
				}
			}
		}
	}
}
