package day1;

public class Main {
	public static void  main (String[] args) {
		int jeff = 12;
		char i = 'i';
		float phi = 3.14f;
		double ipk = 3.7;
		String ganteng = "Achenk";
		boolean benar = true;
		
		System.out.println(jeff);
		System.out.println(phi);
		
		String nama = "Kristy";
		String alamat = "Bogor";
		int usia = 24;
		double tinggi = 173;
		
		System.out.println("Nama = " + nama);
		System.out.println("Alamat = " + alamat);
		System.out.println("Usia = " + usia);
		System.out.println("Tinggi badan = " + tinggi);
		
		//operasi matematika : +, -, /, *, %, ^
		//operasi logika : ==, !=, <, <=, >, >=, &&, ||
		//operasi penugasan : =, +=, ++, etc...
		
		int nilaisatu, nilaidua, nilaitiga;
		nilaisatu = 100;
		nilaidua = 20;
				
		
		nilaisatu=nilaisatu+nilaidua; //nilai satu = 5 + 3 = 8
		nilaidua=nilaisatu-nilaidua; //nilai dua = 8 - 3 = 5
		nilaisatu=nilaisatu-nilaidua; //nilai satu = 8 - 5 = 3
		
		System.out.println("Nilai Satu : " + nilaisatu);
		System.out.println("Nilai Dua : " + nilaidua);
		
	}
}
