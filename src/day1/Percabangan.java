package day1;

import java.util.Scanner;

public class Percabangan {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//Batch 298 lulus jika memiliki nilai lebih besar
		//atau sama dengan 75
		
		System.out.println("Masukkan nama siswa: ");
		String nama = input.next(); //jika pakai nextLine di setelah tipe integer maka input data akan di skip
		//untuk mengatasinya menggunakan input.nextLine
		System.out.println("Masukkan nilai siswa: ");
		int nilai= input.nextInt();
		System.out.println("Masukkan nilai softskill siswa: ");
		int nilaiSoftSkill = input.nextInt();
		
		input.nextLine(); //karena ada perubahan tipe data dari
		//string ke integer digunakan input.nextLine() untuk perubahan tipe data lagi ke String
		System.out.println("Masukkan Batch: ");
		String batch = input.nextLine(); //input.nextline() digunakan untuk yang pakai spasi
		String grade = "";
		String kelulusan = "";
		
		if(nilai >=90 && nilaiSoftSkill >=3) {
			grade = "A";
			kelulusan = "LULUS";
		} else if(nilai >= 85 && nilaiSoftSkill >=3) {
			grade = "B+";
			kelulusan = "LULUS";
		} else if(nilai >= 80 && nilaiSoftSkill >=3) {
			grade = "B";
			kelulusan = "LULUS";
		} else if(nilai >=75 && nilaiSoftSkill >=3) {
			grade = "C+";
			kelulusan = "LULUS";
		} else if(nilai >=70 && nilaiSoftSkill >= 3) {
			grade = "C";
			kelulusan = "REMEDIAL";
		} else {
			grade = "D";
			kelulusan = "TIDAK LULUS";
		}
		System.out.println(nama + " " + batch + " dengan grade " + grade + " dinyatakan " + kelulusan);
	}
}
