package dayy14;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter the number case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.print("Continue? ");
			answer = input.next();
		}

	}

	@SuppressWarnings("deprecation")
	private static void soal1() throws ParseException {
		// mary mendapatkan libur setiap x hari, sedangkan Susan mendapatkan libur
		// setiap y hari
		// jika mereka terakhir mendapatkan libur di hari yang sama pada tanggal z,
		// kapan tanggal terdekat mereka akan libur bersama kembali?
		// input : int x, y; date/varchar z
		// output: tanggal libur bersama selanjutnya
		// contoh: x=3, y=2, z=25 Februari 2020
		// output: 8 maret 2020 (jangan lupa kabisat)

		System.out.println("Penyelesaian soal latihan logic nomor 1");
		System.out.println();

		System.out.print("Masukan libur Mary: ");
		int x = input.nextInt();
		int mary = x + 1;
		System.out.print("Masukan libur Susan: ");
		int y = input.nextInt();
		int susan = y + 1;
		input.nextLine();
		System.out.print("Masukan tanggal Mary dan Susan bertemu (dd MMMM yyyy): ");
		String tgl = input.nextLine();

		String tanggalpertama = tgl.replaceAll(" ", "-");

		SimpleDateFormat df = new SimpleDateFormat("dd-MMMM-yyyy", new java.util.Locale("id"));

		Date datepertama = df.parse(tanggalpertama);
		long tanggalpertama1 = datepertama.getTime();

		String date = "";
		
//		String[] bulan1 = {"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"};
//		String[] bulan2 = {"January","February","March","April","May","June","July","August","September","October","November","December"};
		String[] arraytgl = tgl.split(" ");
		

		// untuk mencari kpknya, kpk digunakan untuk menentukan libur bersama
		int kpk = 0;
		int i = 0;
		while (i!=susan) {
			kpk = kpk + mary;
			if (kpk % susan == 0) {
				break;
			}
			i++;
		}
		// konversi kpk
		long milisekonkpk = kpk * 24 * 60 * 60 * 1000;

		// untuk validitas kalender
		int month = datepertama.getMonth();
		
		// tahun kabisat
		if (Integer.parseInt(arraytgl[2]) % 400 == 0 || Integer.parseInt(arraytgl[2]) % 4 == 0) {
			if (month == 1 || month == 3 || month == 5 || month == 7 || month == 9 || month == 10 || month == 12) {
				if (Integer.parseInt(arraytgl[0]) < 0 || Integer.parseInt(arraytgl[0]) > 31) {
					return;
				}
			}
			else if (month == 2) {
				if (Integer.parseInt(arraytgl[0]) < 0 || Integer.parseInt(arraytgl[0]) > 29) {
					return;
				}
			}
			else if (month == 4 || month == 6 || month == 9 || month == 11) {
				if (Integer.parseInt(arraytgl[0]) < 0 || Integer.parseInt(arraytgl[0]) > 30) {
					return;
				}
			}

		}
		else  {
			if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
				if (Integer.parseInt(arraytgl[0]) < 0 || Integer.parseInt(arraytgl[0]) > 31) {
					return;
				}
			}
			else if (month == 2) {
				if (Integer.parseInt(arraytgl[0]) < 0 || Integer.parseInt(arraytgl[0]) > 28) {
					return;
				}
			}
			else if (month == 4 || month == 6 || month == 9 || month == 11) {
				if (Integer.parseInt(arraytgl[0]) < 0 || Integer.parseInt(arraytgl[0]) > 30) {
					return;
				}
			}

		}
		long nexttanggal = tanggalpertama1 + milisekonkpk;

		Date result = new Date(nexttanggal);
		date = date + (String) df.format(result).replaceAll("-", " ");
		
		System.out.println("Tanggal bertemu selanjutnya adalah " + date);

//		if (x != y) {
//			int mary = x + 1;
//			int susan = y + 1;
//
//			int liburkeduanya = mary * susan;
//			long liburkedua2 = liburkeduanya * 24 * 60 * 60 * 1000;
//
//			long nexttanggal = tanggalpertama1 + liburkedua2;
//
//			Date result = new Date(nexttanggal);
//			date = date + (String) df.format(result).replaceAll("-", " ");
//
//		} else if (x == y) {
//			int liburkeduanya = x+1;
//			long liburke2 = liburkeduanya * 24 * 60 * 60 * 1000;
//			long nexttgl = tanggalpertama1 + liburke2;
//			
//			Date result = new Date(nexttgl);
//			date = date + (String) df.format(result).replaceAll("-", " ");;
//		}
//
//		System.out.println("Tanggal bertemu selanjutnya adalah : " + date);
	}

	private static void soal2() {
		// Urutkan huruf/ karakter dari gabungan beberapa kata atau sebuah kalimat
		// sesuai dengan abjad alfabet
		// input : gabungan beberapa kata atau sebuah kalimat
		// Constraints :
		// - mengandung huruf vokal dan konsonan
		// - urut dan pisahkan huruf/ karakter tersebut
		// - kelompokan huruf yang sama
		// - diproses sebagai huruf kecil (spasi diabaikan)

		// output : huruf yang sama dikelompokan menjadi satu dan dipisahkan dengan
		// tanda "-

		// input n : Sample Case
		// output : aa - c - ee - l - m - p - ss

		System.out.println("Penyelesaian soal latihan nomor 2");
		System.out.println();

		System.out.print("Masukan kata : ");
		String kata = input.nextLine();

		kata = kata.replace(" ", "").toLowerCase();

		char[] charkata = kata.toCharArray();

		for (int i = 0; i < charkata.length; i++) {
			for (int j = 0; j < charkata.length; j++) {
				if (charkata[i] < charkata[j]) {
					char temp = charkata[i];
					charkata[i] = charkata[j];
					charkata[j] = temp;
				}
			}
		}

		for (int i = 0; i < charkata.length; i++) {
			if (i == charkata.length - 1) {
				System.out.print(charkata[i]);
				break;
			} else if (charkata[i] == charkata[i + 1]) {
				System.out.print(charkata[i]);
			} else {
				System.out.print(charkata[i] + " - ");
			}

		}

		for (int i = 0; i < charkata.length; i++) {

		}
	}

}
