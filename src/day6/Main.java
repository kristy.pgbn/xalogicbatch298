package day6;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}

	}

	private static void soal1() {
		System.out.println("Penyelesaian Soal 1");
		
		//bilangan Fibonaci 1,1,2,3,5,8

		System.out.print("Masukan N: ");
		int n = input.nextInt();

		int f0 = 1;
		int f1 = 1;
		int fn = 0;
		for (int i = 0; i < n; i++) {
			if (i == 0) {
				fn = f0;  // fn = 1,
			} else {
				fn = f0; // fn = 1				2
				f0 = f0 + f1; // f0 = 1+1 =2	2+1 =3
				f1 = fn; // f1 = 1				2
			}

			if (i != n - 1) {
				System.out.print(fn + ","); //fn = 1,1,2,
			} else {
				System.out.println(fn);
			}

		}

	}

	private static void soal2() {
		System.out.println("Penyelesaian soal nomor 2");

		System.out.println("Masukan N:");
		int n = input.nextInt();

		int n1 = 1;
		int n2 = 1;
		int n3 = 1;
		int barisan = 0;
		for (int i = 0; i < n; i++) {
			if (i == 0 || i == 1) {
				barisan = n2; 			// output = 1,1,1,3,
			} else {
				barisan = n1; 			// barisan	= 1 			3 			5
				n1 = n1 + n2 + n3; 		// n1 		= 1+1+1 = 3		3+1+1=5		3+1+3=9
				n2 = n3; 				// n2 		= 1 			1 			3
				n3 = barisan; 			// n3		= 1 			3 			5
			}
			if (i != n) {
				System.out.print(barisan + ",");
			} else {
				System.out.print(barisan);
			}
		}

	}

	private static void soal3() {

		System.out.println("Penyelesaian soal nomor 3");

		System.out.print("Masukan N: ");
		int n = input.nextInt();
		String prima = "";
		for (int i = 2; i < n; i++) { 		// 2 		i=3 	i=4 	i=5 	
			int prim = 1; 					// 1 		1 		1 		1 		
			for (int j = 2; j < i; j++) { 	// false 	true 	true 	true
				if (i % j == 0) { 			// 			false 	0		false
					prim = 0; //
				}
			}
			if (prim == 1) {
				if(i==n-1 || i==n-2|| i==n-3) {
					prima = prima + i;		// 2		3				 5
				} else {
					prima = prima + i + ",";
				}
			}
	}
		System.out.println(prima);
	}

	private static void soal4() {
		System.out.println("Penyelesaian soal nomor 4");

		System.out.print("Masukan waktu: ");
		String waktu = input.nextLine().toUpperCase();
		
		String[] waktusplit = waktu.split(":"); //membagi waktu menjadi 3 bagian, setelah tanda ":" maka waktu dipecah
		
		int[] waktusplit1=new int[waktusplit.length];
		
		for(int i=0; i<waktusplit.length; i++) {
			if(i==waktusplit.length-1) {
				waktusplit[i] = waktusplit[i].substring(0,2);  //mengambil 2 elemen di depan
			} else {
				waktusplit[i] = waktusplit[i];
			}
		}
		
		String subwaktu = waktu.substring(8);
		String subwaktu1 = "";
		
		if(subwaktu.equals("PM")) {
			String sementara="";
			int time =0;
			for(int j = 0; j<waktusplit.length; j++) {
				if(j==0) {
					waktusplit1[j] = Integer.parseInt(waktusplit[j]);
					if(waktusplit1[j]<12) {
						waktusplit1[j] = waktusplit1[j] + 12;
						time = time+waktusplit1[j];
						sementara = time+"";
					} else {
						sementara=waktusplit1[j]+"";
					}
					
				} else {
					sementara=waktusplit[j];
				}
				if(j==waktusplit.length-1) {
					System.out.print(sementara);
				} else {
					System.out.print(sementara+":");
				}
				
			}
		} else {
			if(waktusplit[0].equals("12")) {
				waktusplit[0] = "00";
				subwaktu1=waktusplit[0]+waktu.substring(2,8);
			} else {
				subwaktu1 = waktu.substring(0,8);
			}
			System.out.println(subwaktu1);
		}
	}

	private static void soal5() {
		System.out.println("Penyelesaian soal nomor 7");
		
		System.out.print("Masukan nilai: ");
		int n = input.nextInt();
		
		int prima = 2;
		int hasil = 0;
		
		while(n!=1) {
			hasil=prima;
			if(hasil%prima==0) {
				hasil=n/prima;
				System.out.println(""+n+"/"+prima+"="+hasil);
				n=hasil;
			}
			else {
				prima++;
			}
		}
		
		System.out.println();

	}

	private static void soal6() {
		System.out.println("Penyelesaian soal nomor 6");
		
		System.out.println("1. Jarak dari Toko ke Customer 1 = 2KM");
		System.out.println("2. Jarak dari Customer 1 ke Customer 2 = 500 M");
		System.out.println("3. Jarak dari Customer 2 ke Customer 3 = 1.5KM");
		System.out.println("4. Jarak dari Customer 3 ke Customer 4 = 300M");
		
		System.out.println("Masukan rute mana saja yang ditempuh: ");
		String rute = input.nextLine();
		
		String[] arrayrute = rute.split(" ");
		double[] jarak = {2,0.5,1.5,0.3};
		double hasil =0;
		double bensin = 2.5;
		
		for(int i=0; i<arrayrute.length; i++) {
			hasil = hasil + jarak[Integer.parseInt(arrayrute[i])-1];
			if(i != arrayrute.length-1) {
				System.out.print(jarak[Integer.parseInt(arrayrute[i])-1] + " + ");
			} else {
				System.out.print(jarak[Integer.parseInt(arrayrute[i])-1] + " = " + hasil + " KM ");
			}
		}
		bensin = Math.ceil(hasil/bensin);
		System.out.println();
		System.out.println("Bensin sebanyak " + bensin);
	}

	private static void soal7() {
		System.out.println("Penyelesaian soal nomor 7");
		
		System.out.println();
		System.out.print("Masukan inputan : ");
		String inputan = input.next();
		
		char[] arrayinputan = inputan.toUpperCase().toCharArray();
		int hitung =0;
		for(int i=0; i<arrayinputan.length; i+=3) {
			if(arrayinputan[i] != 'S') {
				hitung++;
			}
			if(arrayinputan[i+1] != 'O') {
				hitung++;
			}
			if(arrayinputan[i+2] != 'S') {
				hitung++;
			}
		}
		System.out.println(hitung);
	}

}
