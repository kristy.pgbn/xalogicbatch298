package day7;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case (3-10): ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal3() {
		System.out.println("Penyelesaian soal LMS nomor 3");

		System.out.println("Masukan N: ");
		int n = input.nextInt();
		int total = 0;

		for (int i = 0; i < n; i++) {
			System.out.print("Masukan elemen ke-" + (i + 1) + " ");
			int x = input.nextInt();
			total = total + x;
		}
		System.out.println();
		System.out.print(total);
	}

	private static void soal4() {
		System.out.println("Penyelesaian soal LMS nomor 4");

		System.out.println("Masukan panjang baris dan kolom: ");
		int n = input.nextInt();
		int pertama = 0;
		int kedua = 0;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print("Masukan elemen baris ke " + i + " dan kolom ke " + j + " : ");
				int elemen = input.nextInt();
				if (i == j) {
					pertama = pertama + elemen;
				}
				if (i + j == n - 1) {
					kedua = kedua + elemen;
				}

			}
		}
		int total = Math.abs(pertama - kedua);
		System.out.println(total);

	}

	private static void soal5() {
		System.out.println("Penyelesaian soal LMS nomor 5");

		System.out.println("Masukan N: ");
		int n = input.nextInt();
		double negatif = 0;
		double nol = 0;
		double positif = 0;

		for (int i = 0; i < n; i++) {
			System.out.print("Masukan elemen ke " + (i + 1) + " : ");
			int x = input.nextInt();
			if (x < 0) {
				negatif = negatif + 1;
			}
			if (x > 0) {
				positif = positif + 1;
			}
			if (x == 0) {
				nol = nol + 1;
			}
		}

		negatif = negatif / n;
		positif = positif / n;
		nol = nol / n;
		System.out.println(positif);
		System.out.println(negatif);
		System.out.println(nol);
	}

	private static void soal6() {
		System.out.println("Penyelesaian soal LMS nomor 6");

		System.out.print("Masukan N: ");
		int n = input.nextInt();

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i + j >= n) {
					System.out.print("#");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

	private static void soal7() {
		System.out.println("Penyelesaian soal LMS nomor 7");

		System.out.print("Masukan bilangan : ");
		String bil = input.nextLine();

		String[] arraybil = bil.split(" ");

		int maksimum = 0;
		int minimum = 0;
		int total = 0;

		for (int i = 0; i < arraybil.length; i++) {
			total = 0;
			for (int j = 0; j < arraybil.length; j++) {
				if (j != i) {
					total = total + Integer.parseInt(arraybil[j]);
				}
			}
			if (total > maksimum) {
				maksimum = total;
			}
			if (total < maksimum) {
				minimum = total;
			}

		}
		System.out.println(maksimum);
		System.out.println(minimum);

	}

	private static void soal8() {
		System.out.println("Penyelesaian soal LMS nomor 8");

		System.out.println("Masukan banyak lilin: ");
		int n = input.nextInt();

		System.out.println("Masukan tinggi masing-masing lilin: ");
		String tinggi = input.next();

		String[] arraytinggi = tinggi.split(",");
	
		int tertinggi = 0;
		int jumlah = 0;
		
		for (int i = 0; i < arraytinggi.length; i++) {
			if(Integer.parseInt(arraytinggi[i])>tertinggi) {
				tertinggi = Integer.parseInt(arraytinggi[i]);
			}
		}
		
		for (int i = 0; i < arraytinggi.length; i++) {
			if(Integer.parseInt(arraytinggi[i])==tertinggi) {
				jumlah++;
			}
		}
		
		System.out.println(jumlah);
	}

	private static void soal9() {
System.out.println("Penyelesaian soal LMS nomor 9");
		
		System.out.println("Masukan N: ");
		int n = input.nextInt();
		
		long hasil = 0;
		
		for(int i = 0; i<n; i++) {
			System.out.println("Masukan bilangan ke-" + (i+1) + " : ");
			long bil = input.nextLong();
			hasil = hasil + bil;
		}
		System.out.println();
		System.out.println(hasil);
	}
		
		
	
	private static void soal10() {
		System.out.println("Penyelesaian soal LMS nomor 10");
		
		System.out.println("Masukan nilai tahap 1: ");
		String tahap1 = input.nextLine();
		
		System.out.println("Masukan nilai tahap 2: ");
		String tahap2 = input.nextLine();
		
		String[] tahap1split = tahap1.split(" ");
		String[] tahap2split = tahap2.split(" ");
		
		int alice = 0;
		int bob =0;
		
		int[] arraytahap1 = new int [tahap1split.length];
		int[] arraytahap2 = new int [tahap2split.length];
		
		for(int i = 0; i < arraytahap1.length; i++) {
			arraytahap1[i] = Integer.parseInt(tahap1split[i]) ;
		}
		for(int j = 0; j < arraytahap2.length; j++) {
			arraytahap2[j] = Integer.parseInt(tahap2split[j]) ;
			}
			
		for(int a = 0; a < arraytahap1.length; a++) {
			for(int b = 0; b<=a; b++) {
				if(a==b) {
					if(arraytahap1[b] > arraytahap2[b]) {
						alice++;
					} else if(arraytahap1[b] < arraytahap2[b]) {
						bob++;
					}
				}
			}
		}
		System.out.println(alice + "   " + bob);
}
}

