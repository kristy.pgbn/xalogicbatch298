package day3;

import java.util.Scanner;

public class Main {
	public static void main (String[] args) {
		
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan banyak perulangan: ");
		int n = input.nextInt();
		
		for (int i = 0; i < n; i++) { //increment
			System.out.println(i+1);
		}
		System.out.println();
		
		for (int i = n; i > 0; i--) { //decrement
			System.out.println(i);
		}
		System.out.println("Duar mamank!");
	}
}
