package day3;

import java.util.Scanner;

public class whileloop {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukkan banyak perulangan: ");
		int n = input.nextInt();
		System.out.println("=============================================");
		
		int i = 0;
		
		while(i < n) { //increment
			System.out.println(i+1);
			i++;
		}
		System.out.println();
		i = n;
		while( 0 < i) { //decrement
			System.out.println(i);
			i--;
		}
		input.close();
	}

}
