package day3;

import java.util.Scanner;

public class NestedForLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukkan nilai N: ");
		int n = input.nextInt();
		System.out.println("=======================");
		
		int i=0;
		int j=0;
		
		//program segitiga siku-siku rata kiri
		for(i=0; i<n; i++) {
			for(j=0; j<n; j++) {
				if(j<=i) {
					System.out.print("*");
				}
			}
			System.out.println();
		}
		System.out.println();
		
		//program segitiga siku-siku rata kanan
		for(i=0; i<n; i++) {
			for(j=0; j<n; j++) {
				if(i+j>=n-1) {
					System.out.print("*");
				}else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		System.out.println();
		
		//program bintang bentuk X
		for(i=0; i<n; i++) {
			for(j=0; j<n; j++) {
				if(i==j || i+j==n-1) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		System.out.println();
		
		//segitiga rata kiri
		for(i=0; i<n; i++) {
			for(j=0; j<n; j++) {
				if(j>i || i+j >= n) {
					System.out.print(" ");
				} else {
					System.out.print("*");
				}
			}
			System.out.println();
		}
		
	}

}
