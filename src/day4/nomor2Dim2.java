package day4;

import java.util.Scanner;

public class nomor2Dim2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan N: ");
		int n = input.nextInt();
		System.out.print("Masukan satu angka: ");
		int b = input.nextInt();
		
		double k;
		int p;
		
		int[][] nomor1 = new int[2][n];
		for(int i=0; i<2; i++) {
				for(int j=0; j<n; j++) {
					if(i==1) {
						if(j%3==2) { //supaya di index ke 3, 6, 9,... bernilai negatif maka ketika j modulo 3 = 2 hasil pangkat dikali (-1)
							k = Math.pow(b, j);
							p = (int) k;
							nomor1[i][j]= p*-1; 
							System.out.print(nomor1[i][j]);
						} else {
							k = Math.pow(b, j);
							p = (int) k;
							nomor1[i][j]= p;
							System.out.print(nomor1[i][j]);
						}
					}
					else { //akan menampilkan 0, 1, 2, 3,...
						nomor1[i][j] = j;
						System.out.print(nomor1[i][j]);
					}
					System.out.print("\t");
			}
				System.out.println();
		}
	}
}
