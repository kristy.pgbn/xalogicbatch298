package day4;

public class Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arrayAngka = new int[5];
		
		String dua = "2"; //supaya string dua masuk dalam array yang bertipe int maka perlu di convert
		//untuk mengconvert dapat menggunakan Integer.parseInt(variabel bertipe string)
		int tempAngka = Integer.parseInt(dua);
		
		//memasukan data ke dalam array
		arrayAngka[0] = 1;
		arrayAngka[1] = tempAngka; //diganti dari "dua" menjadi tempAngka, karena  sudah berganti menjadi tipe int
		arrayAngka[2] = 3;
		arrayAngka[3] = 4;
		arrayAngka[4] = 5;
		
		//untuk mengakses nilai di suatu array
		int aksesAngka = arrayAngka[1];
		
		//untuk mengakses array secara otomatis dapat digunakan perulangan for	
		for (int i = 0; i < arrayAngka.length; i++) {
			System.out.print(arrayAngka[i] + " ");
		}
		
		System.out.println();
		
		System.out.println("Panjang Array " + arrayAngka.length);
		System.out.println(aksesAngka);
		
		//contoh jika elemen array bertipe integer diubah ke string
		String[] arrayPembelian = new String[5];
		
		int pembelianPertama = 10000;
		int pembelianKedua = 20000;
		
		//cara mengconvert pertama: gunakan String.valueOf(variabel yang bertipe int)
		arrayPembelian[0] = String.valueOf(pembelianPertama);
		//cara mengconvert kedua menggunakan kutip ""
		arrayPembelian[1] = pembelianKedua + "";
		
		System.out.println("Nilai Pembelian Pertama: " + arrayPembelian[0]);
		System.out.println("Nilai Pembelian Kedua: " + arrayPembelian[1]);
	}

}
