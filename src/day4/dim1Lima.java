package day4;

import java.util.Scanner;

public class dim1Lima {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan N:");
		int n = input.nextInt();
		
		String[] array5 = new String[n];
		
		int p = 1;
		for(int i=0; i<n; i++) {
			if(i%3==2) { //akan menampilkan "*" pada saat i=2,5,8,... 
				System.out.print(array5[i] = "*");
				System.out.print("\t");
			}else {
				System.out.print(array5[i] = p +""); //p yang semula bertipe int akan di konversi menjadi tipe String
				System.out.print("\t");
				p=p+4;
			}	
		}
	}

}
