package day4;

import java.util.Scanner;

public class ArrayTwoDimension {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int[][] arrayAngkaDuaDimen = new int[5][5];
		
		int a=13;
		
		arrayAngkaDuaDimen[2][2] = a;
		
		System.out.println(arrayAngkaDuaDimen[2][2]);
		
		System.out.println();
		
		//membuat matrik nxn dengan angka berurut
		System.out.print("Masukan banyak baris: ");
		int p = input.nextInt();
		System.out.print("Masukan banyak kolom: ");
		int q = input.nextInt();
		int[][] arraynew = new int[p][q] ;
		int n = 1;
		for(int i = 0; i<p; i++) {
			for(int j = 0; j<q; j++) {
				arraynew[i][j] = n;
				System.out.print(arraynew[i][j] + "\t");
				n++;
			}
			System.out.println();
		}
	}

}
