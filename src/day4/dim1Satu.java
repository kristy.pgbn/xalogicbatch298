package day4;

import java.util.Scanner;

public class dim1Satu {
	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	
	System.out.print("Masukan N:");
	int n = input.nextInt();
	
	int[] array1 = new int[n];
	int p=1; //bilangan pertama dimulai dari 1
	for(int i = 0; i<n; i++) {
		System.out.print(array1[i]=p); // akan tampil bilangan kelipatan 2 sebanyak n kali
		System.out.print("\t");
		p=p+2; //p akan diperpaharui dari sebelumnya dengan ditambahkan 2
	}
	}
}
