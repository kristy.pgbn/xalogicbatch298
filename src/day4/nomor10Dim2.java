package day4;

import java.util.Scanner;

public class nomor10Dim2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan N: ");
		int n = input.nextInt();
		System.out.print("Masukan satu angka: ");
		int x = input.nextInt();
		
		int y;
		
		int[][] nomor10 = new int[3][n];
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < n; j++) {
				if(i==0) {
					nomor10[i][j] = j;
				} else if(i==1) {
					nomor10[i][j] = j*x;
				} else { //pada baris ketiga akan mencetak bilangan baris ke-2 ditambah bilangan baris ke-1
					y = j*x;
					y = y+j; 
					nomor10[i][j] = y;
				}
				
				System.out.print(nomor10[i][j]);
				System.out.print("\t");
			}
			System.out.println();
		}
	}

}
