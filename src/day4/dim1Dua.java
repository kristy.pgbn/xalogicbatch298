package day4;

import java.util.Scanner;

public class dim1Dua {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukkan N: ");
		int n = input.nextInt();
		
		int[] array2 = new int [n];
		int p=2; //bilangan pertama dimulai dari 2
		for(int i=0; i<n; i++) {
			System.out.print(array2[i] = p); //akan tampil bilangan kelipatan 2 sebanyak n kali
			System.out.print("\t");
			p=p+2;
		}
	}
}
