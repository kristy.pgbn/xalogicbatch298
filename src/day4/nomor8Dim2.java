package day4;

import java.util.Scanner;

public class nomor8Dim2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan N: ");
		int n = input.nextInt();
		
		int[][] nomor8 = new int[3][n];
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < n; j++) {
				if(i==0) {
					nomor8[i][j] = j;
				} else if(i==1) {
					nomor8[i][j] = j*2;
				} else {
					nomor8[i][j] = j*3;
				}
				System.out.print(nomor8[i][j]);
				System.out.print("\t");
			}
			System.out.println();
		}
	}

}
