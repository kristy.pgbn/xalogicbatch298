package day4;

import java.util.Scanner;

public class dim1Empat {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan N: ");
		int n = input.nextInt();
		
		int[] array4 = new int[n];
		
		int p=1;
		for(int i = 0; i<n; i++) {
			System.out.print(array4[i]=p); //menampilkan bilangan kelipatan 4 dimulai dari 1 sebanyak n kali
			System.out.print("\t");
			p=p+4;
		}
	}
}
