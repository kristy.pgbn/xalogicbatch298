package day4;

import java.util.Scanner;

public class nomor6Dim2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan N: ");
		int n = input.nextInt();
		double y; //karena y akan menggunakan operasi matematika yaitu pangkat maka tipe double
		int k;
		
		int[][] nomor6 = new int[3][n];
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < n; j++) {
				if(i==0) {
					nomor6[i][j]=j;
				} else if(i==1){
					y = Math.pow(n, j);
					k = (int) y;
					nomor6[i][j] = k;
				} else {
					if(j==0) {
						y = Math.pow(n, j);//untuk operasi pangkat pakai Math.pow
						k = (int) y; //karena y masih bertipe double mengakibatkan y tidak dapat dimasukan dalam array
									//oleh karena itu dilakukan konversi terhadap y supaya bertipe int dengan diinisialkan sebagai k
						nomor6[i][j] = k;
					} else {
						y = Math.pow(n, j) + j;
						k = (int) y;
						nomor6[i][j] = k;
					}
				}
				System.out.print(nomor6[i][j]);
				System.out.print("\t");
			}
			System.out.println();
		}
		}
}
