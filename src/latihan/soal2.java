package latihan;

import java.util.Scanner;

public class soal2 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan kata : ");
		String kata = input.nextLine();
		String kata1 = kata.toLowerCase().replaceAll(" ", "");
		char[] charkata = kata1.toCharArray();
		
		for(int i = 0; i< kata1.length(); i++) {
			for(int j=0; j< kata1.length(); j++) {
				if(charkata[i] < charkata[j]) {
					char temp = charkata[i];
					charkata[i] = charkata[j];
					charkata[j] = temp;
				}
			}
		}

		System.out.println("vocal : ");
		for(int i = 0 ; i < kata1.length(); i++) {
			char karakter = charkata[i];
			if(karakter == 'a' || karakter == 'i' || karakter == 'u' || karakter == 'e' || karakter == 'o') {
				System.out.print(karakter);				
			}
		}
		System.out.println();
		System.out.println("konsonan: ");
		for(int i = 0; i < kata1.length(); i++) {
			char konsonan = charkata[i];
			if(konsonan == 'a'|| konsonan == 'i' || konsonan == 'u' || konsonan == 'e'|| konsonan == 'o') {
				continue;
			} else {
				System.out.print(konsonan);
			}
		}
		
	}

}
