package latihan;

import java.util.Scanner;

public class Gabungan {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}

	}

	private static void soal1() {
		//angka ganjil dan genap
		System.out.println();
		System.out.println("Masukan N: ");
		int n = input.nextInt();
		
		int[] array = new int[n];
		for(int i =0; i<n; i++) {
			array[i-0] = i+1;
		}
		System.out.println("Ganjil : ");
		for(int i = 0; i<array.length; i++) {
			if(array[i]%2 ==1) {
				System.out.print(array[i] + " ");
			}
		}
		System.out.println();
		System.out.println("Genap : ");
			for(int i = 0; i<n; i++) {
				if(array[i]%2 == 1) {
					continue;
				} else {
					System.out.print(array[i] + " ");
				}
			}
	}

	private static void soal2() {
		//sorting vocal dan konsonan
		System.out.println("Masukan kata : ");
		String kata = input.nextLine();
		String kata1 = kata.toLowerCase().replaceAll(" ", "");
		char[] charkata = kata1.toCharArray();
		
		for(int i = 0; i< kata1.length(); i++) {
			for(int j=0; j< kata1.length(); j++) {
				if(charkata[i] < charkata[j]) {
					char temp = charkata[i];
					charkata[i] = charkata[j];
					charkata[j] = temp;
				}
			}
		}

		System.out.println("vocal : ");
		for(int i = 0 ; i < kata1.length(); i++) {
			char karakter = charkata[i];
			if(karakter == 'a' || karakter == 'i' || karakter == 'u' || karakter == 'e' || karakter == 'o') {
				System.out.print(karakter);				
			}
		}
		System.out.println();
		System.out.println("konsonan: ");
		for(int i = 0; i < kata1.length(); i++) {
			char konsonan = charkata[i];
			if(konsonan == 'a'|| konsonan == 'i' || konsonan == 'u' || konsonan == 'e'|| konsonan == 'o') {
				continue;
			} else {
				System.out.print(konsonan);
			}
		}
	}

	private static void soal3() {
		//Aku adalah "Si Angka 1"
		System.out.println("Masukan N: ");
		int n = input.nextInt();
		int bil = 100;
		int bil1 = 3;
		for (int i =0; i<n; i++) {
			if(i==0) {
				System.out.println(bil + " adalah " + "\"Si Angka 1\"" );
			} else {
				bil=100;
				bil = (int) (bil + Math.pow(bil1, i));
				System.out.println(bil + " adalah " + "\"Si Angka 1\"" );
			}
		}
		
	}
	private static void soal4() {
		//hitung banyak bensin
		System.out.println("1. Toko ke tempat makan = 2 km");
		System.out.println("2. Tempat 1 ke tempat 2 = 500 m");
		System.out.println("3. Tempat 2 ke tempat 3 = 1,5 km");
		System.out.println("4. Tempat 3 ke tempat 4 = 2,5 km");
		
		System.out.println();
		System.out.println("Masukan rute yang dilewati (gunakan koma untuk pemisah) : ");
		String rute = input.next();
		
		String[] arrayrute = rute.split(",");
		double[] jarak = {2,0.5,1.5,2.5};
		double jarakhitung = 0;
		
		for(int i =0; i<arrayrute.length; i++) {
			jarakhitung = jarakhitung + jarak[Integer.parseInt(arrayrute[i])-1];
		}
		
		double jarakbensin = 2.5;
		
		jarakbensin = Math.ceil(jarakhitung/jarakbensin);
		int totalbensin = (int) jarakbensin;
		System.out.println("Bensin yang dibutuhkan adalah " + totalbensin + " liter");
	}
	private static void soal5() {
		System.out.print("Masukan banyak laki-laki dewasa : ");
		int lakilaki = input.nextInt();
		System.out.print("Masukan banyak perempuan dewasa : ");
		int perempuan = input.nextInt();
		System.out.println("Masukan banyak remaja : ");
		int remaja = input.nextInt();
		System.out.println("Masukan banyak anak-anak: ");
		int anak = input.nextInt();
		System.out.print("Masukan banyak balita : ");
		int balita = input.nextInt();
		
		int laki = 0;
		int wanita = 0;
		int rmj = 0;
		int ank = 0;
		int blt =0;
		System.out.println("Ingin tambah inputan? ");
		String answer = input.next();
		
		if(answer.equals("Y") || answer.equals("y")) {
			System.out.println("1. Laki-laki");
			System.out.println("2. Perempuan");
			System.out.println("3. Remaja");
			System.out.println("4. Anak-anak");
			System.out.println("5. Balita");
			System.out.println("Masukan pilihan: ");
			String pilihan = input.next();
			
			String[] arraypilihan = pilihan.split(",");
			for(int i=0; i<arraypilihan.length ; i++) {
				if(Integer.parseInt(arraypilihan[i])==1) {
					System.out.print("Masukan banyak laki-laki: ");
					laki = input.nextInt();
				} else if(Integer.parseInt(arraypilihan[i]) == 2) {
					System.out.print("Masukan banyak perempuan dewasa : ");
					wanita = input.nextInt();
				} else if(Integer.parseInt(arraypilihan[i]) == 3) {
					System.out.println("Masukan banyak remaja : ");
					rmj = input.nextInt();
				} else if(Integer.parseInt(arraypilihan[i]) == 4) {
					System.out.println("Masukan banyak anak-anak: ");
					ank = input.nextInt();
				} else {
					System.out.print("Masukan banyak balita : ");
					blt = input.nextInt();
				}
			}
		}
		double porsi = 0;
		int total = lakilaki + laki + perempuan + wanita + remaja + rmj + anak + ank + balita + blt;
		if(total%2 ==1 && total >5) {
			porsi = ((lakilaki+laki)*2) + ((perempuan + wanita)*1) + ((remaja+rmj)*1)  + ((anak+ank)*0.5) + ((balita+blt)*1);
			porsi = porsi + ((perempuan + wanita)*1);
			System.out.println(porsi + " porsi");
		}else {
			porsi = ((lakilaki+laki)*2) + ((perempuan + wanita)*1) + ((remaja+rmj)*1)  + ((anak+ank)*0.5) + ((balita+blt)*1);
			System.out.println(porsi + " porsi");
		}
	}
	private static void soal6() {
		long uang = 0;
		int pilihan = 0;
		String rekening = "";
		long transfer = 0;
		long sisa =0;
		String kode = "";
		String answer = "Y";
		while(answer.toUpperCase().equals("Y")) {
			System.out.println("Masukan PIN : ");
			String pin = input.next();
			if(pin.length()!=6) {
				System.out.println("PIN yang Anda masukan salah");
			}
			else {
				System.out.println("Masukan uang yang akan disetor: ");
				uang = input.nextLong();
				System.out.println("Pilihan Transfer: ");
				System.out.println("1. Antar Rekening");
				System.out.println("2. Antar Bank");
				System.out.println("Masukan pilihan : ");
				pilihan = input.nextInt();
				if(pilihan ==1) {
					System.out.println("Masukan nomor rekening : ");
					rekening = input.next();
					if(rekening.length()!=10) {
						System.out.println("Nomor rekening yang Anda masukan salah");
					} else {
						System.out.println("Masukan nominal transfer :");
						transfer = input.nextLong();
						sisa = uang - transfer;
						
						System.out.println();
						System.out.println("Transaksi berhasil, saldo Anda saat ini Rp. " + sisa);
					}
				}
				else if(pilihan ==2) {
					System.out.println("Masukan kode Bank : ");
					kode = input.next();
					if(kode.length()!=3) {
						System.out.println("Kode Bank salah");
					}else {
						System.out.println("Masukan rekening tujuan : ");
						rekening = input.next();
						if(rekening.length()!=10) {
							System.out.println("Nomor rekenin yang Anda masukan salah");
						}else {
							System.out.println("Masukan nominal transfer :");
							transfer= input.nextLong();
							long pajak = 7500;
							
							sisa = uang-transfer-pajak;
							System.out.println("Transaksi berhasil, saldo Anda saat ini Rp. " + sisa);
						}
					}
				}
			}
			System.out.println();
			System.out.println("Lanjut transaksi? (y/n)");
			answer = input.next();
			if(answer.toUpperCase().equals("N")) {
				System.out.println("Transaksi selesai");
			}
		}
	}
	private static void soal7() {
		System.out.println("Masukan jumlah kartu : ");
		int kartu = input.nextInt();
		
		String answer = "Y";
		while(answer.toUpperCase().equals("Y")) {
			System.out.println("Masukan jumlah tawaran : ");
			int tawaran = input.nextInt();
			System.out.println("Pilih kotak (A/B) : ");
			String pilihan = input.next();
			
			int randomA = (int) (Math.random()*10);
			int randomB = (int) (Math.random()*10);
			
//			System.out.println(randomA);
//			System.out.println(randomB);
			
			String hasil = "";
			
			if(randomA>randomB) {
				if(pilihan.toUpperCase().equals("A")) {
					kartu = kartu + tawaran;
					hasil = "You Win";
				}
				if(pilihan.toUpperCase().equals("B")) {
					kartu = kartu - tawaran;
					hasil = "You Lose";
				}
			}
			if(randomA<randomB) {
				if(pilihan.toUpperCase().equals("A")) {
					kartu = kartu - tawaran;
					hasil = "You Lose";
				}
				if(pilihan.toUpperCase().equals("B")) {
					kartu = kartu + tawaran;
					hasil = "You Win";
				}
			}
			if(randomA==randomB) {
				if(pilihan.toUpperCase().equals("A") || pilihan.toUpperCase().equals("B")) {
					hasil = "Seri";
				}
			}
			System.out.println(randomA);
			System.out.println(randomB);
			System.out.println(hasil);
			
			System.out.println();
			if(kartu!=0) {
				System.out.println("Lanjut permainan? (Y/N) ");
				answer = input.next();
				if(answer.toUpperCase().equals("N")) {
					System.out.println("Permainan selesai");
				}
			}else {
				System.out.println("Kartu Anda habis!!");
				break;
			}
		}
	}
	private static void soal8() {
		//penjumlahan bilangan ganjil dan genap
		//misal n= 5
		// genap : 0 2 4 6 8
		// ganjil : 1 3 5 7 9
		//output : 1 5 9 13 17
		System.out.println("Masukan panjang deret: ");
		int n = input.nextInt();
		
		int[] arrayganjil = new int[n];
		int[] arraygenap = new int[n];
		int[] total = new int[n];
		
		for(int i =0; i<n; i++) {
			if(i==0) {
				arraygenap[i] = i;
			}else {
				arraygenap[i] = i*2;
			}
		}
		
		for(int i =0; i<n; i++) {
			arrayganjil[i] = arraygenap[i] +1;
		}
		
		for(int i =0; i<n; i++) {
			total[i] = arraygenap[i] + arrayganjil[i];
		}

		for(int i =0; i<n; i++) {
			if(i!=n-1) {
				System.out.print(total[i] + ",");
			}
			else {
				System.out.print(total[i]);
			}
		}
	}
	private static void soal9() {
		//menhitung sinyal gunung dan lembah
		
		System.out.println("Masukan sinyal : ");
		String sinyal = input.next();
		
		sinyal=sinyal.toUpperCase();
		
		char[] charsinyal = sinyal.toCharArray();
		
		int hitunggunung = 0;
		int hitunglembah =0;
		int mdpl =0;
		
		int[] mdplsinyal = new int [sinyal.length()];
		
		for(int i =0; i<sinyal.length(); i++) {
			if(charsinyal[i]=='N') {
				mdpl = mdpl+1;
				mdplsinyal[i] = mdpl;
				if(mdpl == 0) {
					hitunglembah++;
				}
			} else if(charsinyal[i] == 'T') {
				mdpl = mdpl-1;
				mdplsinyal[i] = mdpl;
				if(mdpl ==0) {
					hitunggunung++;
				}
			} else {
				System.out.println("Sinyal tidak sesuai!!");
			}
		}
//		for(int i =0; i<sinyal.length(); i++) {
//			System.out.println(mdplsinyal[i]);
//		}
//		for(int i =0; i<sinyal.length(); i++) {
//			if(i>0 && mdplsinyal[i]==0) {
//				
//			}
//		}
		System.out.println("banyak gunung : " + hitunggunung);
		System.out.println("banyak lembah : " + hitunglembah);
	}
	private static void soal10() {
		//saldo akhir setelah order setealh dapet diskon dan cb
		//diskon 50% (maks 100k, min order 40k)
		//cb10% (maks 30)
		System.out.println("Masukan saldo OPO :");
		int saldo = input.nextInt();
		
		int hargakopi = 18000;
		
		int n =0;
		double total = 0;
		boolean lanjut = true;
		while(lanjut == true) {
			if(total+hargakopi>saldo) {
				lanjut = false;
			}
			n++;
			total = n*hargakopi;
			if(total >=40000) {
				total = total*0.5;
			} else if(total >100000) {
				total = total - 100000;
			}
			
		}
		
		double sisa = saldo-total;
		double cb = total*0.1;
		if(cb>30000) {
			cb = 30000;
			sisa = sisa + cb;
		} else {
			sisa = sisa + cb;
		}
		
		System.out.println("Jumlah cup = " + n + "; Saldo akhir " + (int)sisa);
	}
		

}
