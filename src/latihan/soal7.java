package latihan;

import java.util.Scanner;

public class soal7 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan jumlah kartu : ");
		int kartu = input.nextInt();
		
		String answer = "Y";
		while(answer.toUpperCase().equals("Y")) {
			System.out.println("Masukan jumlah tawaran : ");
			int tawaran = input.nextInt();
			System.out.println("Pilih kotak (A/B) : ");
			String pilihan = input.next();
			
			int randomA = (int) (Math.random()*10);
			int randomB = (int) (Math.random()*10);
			
//			System.out.println(randomA);
//			System.out.println(randomB);
			
			String hasil = "";
			
			if(randomA>randomB) {
				if(pilihan.toUpperCase().equals("A")) {
					kartu = kartu + tawaran;
					hasil = "You Win";
				}
				if(pilihan.toUpperCase().equals("B")) {
					kartu = kartu - tawaran;
					hasil = "You Lose";
				}
			}
			if(randomA<randomB) {
				if(pilihan.toUpperCase().equals("A")) {
					kartu = kartu - tawaran;
					hasil = "You Lose";
				}
				if(pilihan.toUpperCase().equals("B")) {
					kartu = kartu + tawaran;
					hasil = "You Win";
				}
			}
			if(randomA==randomB) {
				if(pilihan.toUpperCase().equals("A") || pilihan.toUpperCase().equals("B")) {
					hasil = "Seri";
				}
			}
			System.out.println(randomA);
			System.out.println(randomB);
			System.out.println(hasil);
			
			System.out.println();
			if(kartu!=0) {
				System.out.println("Lanjut permainan? (Y/N) ");
				answer = input.next();
				if(answer.toUpperCase().equals("N")) {
					System.out.println("Permainan selesai");
				}
			}else {
				System.out.println("Kartu Anda habis!!");
				break;
			}
			
		}
		
		

	}

}
