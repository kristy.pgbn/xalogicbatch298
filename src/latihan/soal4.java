package latihan;

import java.util.Scanner;

public class soal4 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("1. Toko ke tempat makan = 2 km");
		System.out.println("2. Tempat 1 ke tempat 2 = 500 m");
		System.out.println("3. Tempat 2 ke tempat 3 = 1,5 km");
		System.out.println("4. Tempat 3 ke tempat 4 = 2,5 km");
		
		System.out.println();
		System.out.println("Masukan rute yang dilewati (gunakan koma untuk pemisah) : ");
		String rute = input.next();
		
		String[] arrayrute = rute.split(",");
		double[] jarak = {2,0.5,1.5,2.5};
		double bensin = 2.5;
		double jarakhitung = 0;
		
		for(int i =0; i<arrayrute.length; i++) {
			jarakhitung = jarakhitung + jarak[Integer.parseInt(arrayrute[i])-1];
		}
		
		double jarakbensin = 2.5;
		
		jarakbensin = Math.ceil(jarakhitung/jarakbensin);
		int totalbensin = (int) jarakbensin;
		System.out.println("Bensin yang dibutuhkan adalah " + totalbensin + " liter");
	}

}
