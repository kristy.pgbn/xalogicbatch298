package latihan;

import java.util.Scanner;


public class soal8 {

	public static void main(String[] args) {
		Scanner inputScanner = new Scanner(System.in);
		
		System.out.println("Masukan panjang deret: ");
		int n = inputScanner.nextInt();
		
		int[] arrayganjil = new int[n];
		int[] arraygenap = new int[n];
		int[] total = new int[n];
		
		for(int i =0; i<n; i++) {
			if(i==0) {
				arraygenap[i] = i;
			}else {
				arraygenap[i] = i*2;
			}
		}
		
		for(int i =0; i<n; i++) {
			arrayganjil[i] = arraygenap[i] +1;
		}
		
		for(int i =0; i<n; i++) {
			total[i] = arraygenap[i] + arrayganjil[i];
		}

		for(int i =0; i<n; i++) {
			if(i!=n-1) {
				System.out.print(total[i] + ",");
			}
			else {
				System.out.print(total[i]);
			}
		}
	}

}
